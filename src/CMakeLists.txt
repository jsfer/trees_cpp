set(TREES_SOURCES
    main.cpp
)

add_executable(trees ${TREES_SOURCES})
target_include_directories(trees PRIVATE ${PROJECT_SOURCE_DIR}/src)

target_link_libraries(trees fmt::fmt)

install(TARGETS trees RUNTIME DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
