#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TREES_TEST_MODULE
#include <boost/test/unit_test.hpp>

#include <resource.h>

BOOST_AUTO_TEST_SUITE(TreesTestSuite)

BOOST_AUTO_TEST_CASE(TestCase1) {
	auto res = Resource();
	BOOST_CHECK_EQUAL(res.message, "hi");
}

BOOST_AUTO_TEST_SUITE_END()

